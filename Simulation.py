# -*- coding: utf-8 -*-
from tkinter import *
import datetime
import random
import argparse
import time
from tkinter.ttk import Combobox
#----------------------------------------------------
#       QUELQUES FONCTIONS UTILES
#------------------------------------------------------
'''
    etat de chaque cases 
        0 = rien
        1 = foret
        3 = feu
        4 = angle 
        5 = feu sur rien
'''
def create_foret():
    '''
        Cette fonction permet d'initialiser la fôret de façon aleatoire
    '''
    x=0
    y=0
    n=0
    for i in range(args.rows):  
        for j in range(args.cols):
            if(args.afforestation >= random.random()) :
                    x=i*args.cell_size
                    y=j*args.cell_size
                    color= 'green'
                    case[i][j]= monCanvas.create_rectangle(x, y, x+args.cell_size, y+args.cell_size, fill=color)
                    etat_case[i][j] = 1
                    n+=1
            else:
                x=i*args.cell_size
                y=j*args.cell_size
                color = '#73654c'
                case[i][j]= monCanvas.create_rectangle(x, y, x+args.cell_size, y+args.cell_size, fill=color)
                etat_case[i][j] = 0
                 
def initialisation():
    '''
     Cette fonction permet de creer les grilles dans le canvas afin d'y placer les arbres
    '''
    cor_x =0  #position verticale
    cor_y =0  #position verticale

    #creation des separations verticales
    for i in range(args.rows) :
        if cor_x!=canvas_height :
            monCanvas.create_line(cor_x,0,cor_x,canvas_height,width=1,fill='black')
            cor_x+=args.cell_size
    #creation des separations horizontales
    for j in range(canvas_width):
        if cor_y!=canvas_width :
            monCanvas.create_line(0,cor_y,canvas_width,cor_y,width=1,fill='black')
            cor_y+=args.cell_size


def click_gauche(event):
    '''
        Cette fonction mettra en feu sur une cellule
    '''
    abs = 0
    ordo =0
    x = event.x -(event.x%args.cell_size)  
    y = event.y -(event.y%args.cell_size)
    for i in range(0, x, args.cell_size) :
            abs+=1
    for j in range(0, y, args.cell_size) :
            ordo+=1
    if etat_case[abs][ordo] == 1 :  
        etat_case[abs][ordo] = 3    #si c'est un arbre l'etat pass à 3
    else :
        etat_case[abs][ordo] = 5  #sinon etat passe à 5 
        monCanvas.itemconfig(case[abs][ordo], fill = '#f29b02')

def click_droit(event):
    '''
        Cette fonction enlèvera le feu d'une cellule et la remplacer par la terre
    '''
    abs=0
    ordo=0
    x = event.x -(event.x%args.cell_size)
    y = event.y -(event.y%args.cell_size)
    for i in range(0, x, args.cell_size) :
            abs+=1
    for j in range(0, y, args.cell_size) :
            ordo+=1
    etat_case[abs][ordo] = 1
    monCanvas.itemconfig(case[abs][ordo], fill = 'green')

#---------------------------------------------------------
#   FONCIONS PRINCIPALES
#--------------------------------------------------------
def cas_speciaux():
    '''
        Dans cette fonction il sera question de parcourit les cases et de mettre en feu les cases voisines si ce sont 
        des forêts/arbres
    '''
    #cas_speciaux dans laquelle la propagation s'arrete
    for i in range(args.cols):
        etat_case[0][i]=4
        flag = False
    for i in range(args.rows):
        etat_case[i][0]=4
        flag = False
    for i in range(args.cols):
        etat_case[-1][i]=4
        flag = False
    for i in range(args.rows):
        etat_case[i][-1]=4
        flag = False

def cendre():
     #mise en gris des parcelles
    global t
    for i in range(args.rows):
        for j in range(args.cols):
            if etat_case[i][j]== 6:
                color= '#f29b02'
                monCanvas.itemconfig(case[i][j],fill=color)
            if etat_case[i][j]== 5 :
                color= '#f29b02'
                monCanvas.itemconfig(case[i][j],fill=color)
            if etat_case[i][j]== 4 :
                color= '#73654c'
                monCanvas.itemconfig(case[i][j],fill=color)

def neumann():
    '''
        mise en feu sur les 4 cotés voisines
    '''
    sup=0
    nb=0
    t=1
    cas_speciaux()
    for i in range(args.cols):
        for j in range(args.rows):
            if etat_case[i][j]== 3 :   #si il y'a du feu dans un arbre alors on regarde autour
                nb+=1
                monCanvas.itemconfig(case[i][j],fill='#f29b02')
                etat_case[i][j]=6

                if etat_case[i+1][j] == 1:
                    sup+=1
                    monCanvas.itemconfig(case[i+1][j],fill='red')
                    etat_case[i+1][j]=3
                    monCanvas.itemconfig(case[i+1][j],fill='gray')
                
                if etat_case[i-1][j] == 1:
                    sup+=1
                    monCanvas.itemconfig(case[i-1][j],fill='red')
                    etat_case[i-1][j]=3

                if etat_case[i][j+1] == 1:
                    sup+=1
                    monCanvas.itemconfig(case[i][j+1],fill='red')
                    etat_case[i][j+1]=3


                if etat_case[i][j-1]== 1:
                    sup+=1
                    monCanvas.itemconfig(case[i][j-1],fill='red')
                    etat_case[i][j-1]=3
       
    cendre()


def moore():
    '''
         cette fonction mettra en fera les 8 cotés voisines d'une parcelle arbre
    '''
    cas_speciaux()
    for i in range(args.cols):
        for j in range(args.rows):
            if etat_case[i][j] == 3 :
                monCanvas.itemconfig(case[i][j],fill='#f29b02')
                etat_case[i][j]=6

                if etat_case[i+1][j] == 1:
                    monCanvas.itemconfig(case[i+1][j],fill='red')
                    etat_case[i+1][j]=3
            

                if etat_case[i-1][j] == 1:
                    monCanvas.itemconfig(case[i-1][j],fill='red')
                    etat_case[i-1][j]=3

                if etat_case[i][j+1] == 1:
                    monCanvas.itemconfig(case[i][j+1],fill='red')
                    etat_case[i][j+1]=3
            
                if etat_case[i][j-1] == 1:
                    monCanvas.itemconfig(case[i][j-1],fill='red')
                    etat_case[i][j-1] = 3
 
                
                if etat_case[i-1][j-1] == 1:
                    monCanvas.itemconfig(case[i-1][j-1],fill='red')
                    etat_case[i-1][j-1]=3
          

                if etat_case[i-1][j+1] == 1:
                    monCanvas.itemconfig(case[i-1][j+1],fill='red')
                    etat_case[i-1][j+1]=3
          
                
                if etat_case[i+1][j+1] == 1:
                    monCanvas.itemconfig(case[i+1][j+1],fill='red')
                    etat_case[i+1][j+1] = 3
              

                if etat_case[i+1][j-1] == 1:
                    monCanvas.itemconfig(case[i+1][j-1],fill='red')
                    etat_case[i+1][j-1] = 3
             
    cendre()

def choix():
    global continuer,t
    if continuer == True :
        if regle.get()== 0:
            btnplay.config(state=NORMAL)
            btnpause.config(state=NORMAL) 
            t.set("Simulation en cours....")
            moore()
        else :
            btnplay.config(state=NORMAL)
            btnpause.config(state=NORMAL)
            neumann()
    root.after(vitesse,choix)

def pause():
    global continuer,t
    continuer = False
    t.set("Simulation en pause")
    
def play():
    global continuer, t
    continuer= True
    choix()
    t.set("Simulation en cours...")

# ----------------------------------------------------------------
# Corps du programme
# ----------------------------------------------------------------
if __name__ == '__main__' : 
    #-------< parametre Programme >---------------------------
    parser=argparse.ArgumentParser(description="This a usefull description")
    parser.add_argument("-rows", help="saisir le nombre de ligne ", type=int, default=30)
    parser.add_argument("-cols", help="saisir le nombre de colonne", type = int, default=30)
    parser.add_argument("-cell_size", help="saisir la taille d'une cellule", type = int, default=20)
    parser.add_argument("-afforestation", help="saisir pourcentage de  boisement(compris entre 0 et 1)", type = float, default=0.5)
    args = parser.parse_args()

    #-------<Fenetre ROOT>------------------------------------------------
    root = Tk()
    root.title("Feu de Forêt")
    root.minsize(780,780)
    titre=Label(root, text = "Simulation Feu de Forêt ", font=("courrier", 15))
    titre.pack(side=TOP)

    #-------------------< Variables global >-----------------------------------------
    global etat_case, case
    etat_case= [[0] * args.cols for i in range(args.rows)]  #tableau de l'etat de chaque case
    case= [[0] * args.cols for i in range(args.rows)] #permet d'identifier chaque case

    #--------<CANVAS>-----------------------------------------
    global canvas_height, canvas_width, vitesse, continuer, t
    regle = IntVar()
    t=StringVar()
    continuer = True
    vitesse =2000
    canvas_width= args.cols*args.cell_size      #largeur du canvas
    canvas_height = args.rows*args.cell_size    #hauteur du canvas
    monCanvas = Canvas(root, width=canvas_width, height=canvas_height, bg='white', bd=0, highlightthickness=0)
    monCanvas.pack(expand=YES)
    monCanvas.place(relx=0.5,rely=0.5,anchor=CENTER)
    monCanvas.bind("<Button-1>", click_gauche)
    monCanvas.bind("<Button-3>", click_droit)

    #-----------<QLQs Boutons>----------------------------------
    btn = Frame(root)
    btn_quit = Button(btn, text='Quitter', command = root.destroy).grid(row=2, column=3, sticky='', padx=50)
    btn_re = Button(btn, text='Régénérer la forêt', command = create_foret).grid(row=2, column=0 , sticky='', padx=50)
    choix1 = Radiobutton(btn,text="Von Neumann", variable =regle,value=1).grid(row=1, column=0)
    choix2= Radiobutton(btn,text="Moore", variable =regle, value=0).grid(row=1, column=3)
    lbl = Label(btn, textvariable= t).grid(rows=1, column=1)
    btnpause = Button(btn, text= "Pause", command=pause)
    btnpause.grid(row=2, column=2, sticky='', padx=50)
    btnplay = Button(btn, text= "Play", command=play)
    btnplay.grid(row=2, column=1, sticky='', padx=50)
    btnplay.config(state=DISABLED)
    btnpause.config(state=DISABLED)
    btn.pack(side=BOTTOM, pady=10)
   
    #---------< PROGRAMME >------------------------------------
    initialisation()
    create_foret()
    choix()
    root.mainloop()